var SLIDE_CONFIG = {
   // Slide Settings
   settings: {
     title: 'ESCOVA', 
     subtitle: 'ElasticSearch Correctness and perfOrmance Validator', 
     useBuilds: true, // Default: true. False will turn off slide animation builds. 
     usePrettify: true, // Default: true 
     enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
     enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
     favIcon: 'images/fosdem-icon.jpg', 
     fonts: [
       'Open Sans:regular,semibold,italic,italicsemibold',
       'Source Code Pro'
     ],
   }, 
 
   // Author information
   presenters: [{
     name: 'Santiago Saavedra', 
     company: 'OpenShine, SL', 
     gplus: '', 
     twitter: 'ssice', 
     www: 'https://ssaavedra.eu', 
     github: 'http://github.com/openshine/escova', 
   }]
};